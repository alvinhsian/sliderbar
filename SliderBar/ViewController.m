//
//  ViewController.m
//  SliderBar
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/16.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _valueLabel.text = [NSString stringWithFormat:@"%.2f", _mySlider.value];
    
//    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
//    
//    _mySlider.transform = trans;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sliderValueChange:(id)sender {
    _valueLabel.text = [NSString stringWithFormat:@"%.2f", _mySlider.value];
}
- (IBAction)add:(id)sender {
    _mySlider.value = _mySlider.value + 0.1;
    _valueLabel.text = [NSString stringWithFormat:@"%.2f", _mySlider.value];
}

- (IBAction)less:(id)sender {
    _mySlider.value = _mySlider.value - 0.1;
    _valueLabel.text = [NSString stringWithFormat:@"%.2f", _mySlider.value];
}
@end
