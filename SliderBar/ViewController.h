//
//  ViewController.h
//  SliderBar
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/16.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISlider *mySlider;

- (IBAction)sliderValueChange:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

- (IBAction)add:(id)sender;

- (IBAction)less:(id)sender;

@end

